import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE
from sklearn.metrics import confusion_matrix,precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier
import itertools
from matplotlib import pyplot as plt
import os

#Function plot_sonfusion_matrix from https://blog.csdn.net/mago2015/article/details/86426942
def plot_sonfusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment='center', color='white' if cm[i, j] > thresh else 'black')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predict label')
    
####################Load Data#####################
train_X = pd.read_csv('/content/train_X.csv')
test_X = pd.read_csv('/content/test_X.csv')
train_y = pd.read_csv('/content/train_y.csv')
test_y = pd.read_csv('/content/test_y.csv')

###################SMOTE Oversampling########################
smo = SMOTE(random_state=42, n_jobs=-1)
x_sampling, y_sampling = smo.fit_sample(train_X, train_y)
print(x_sampling.shape)

####################Classification Model#####################

model = DecisionTreeClassifier().fit(x_sampling, y_sampling)

####################Compute Prediction#######################
pred_y = model.predict(test_X)

####################Compute Accuracy, Precision, Recall, F-score########################
print('Accuracy:'+ str(accuracy_score(test_y, pred_y)))
Precision,Recall,Fscore,none = precision_recall_fscore_support(test_y, pred_y, pos_label=1, average='binary') 
print('Precision:'+ str(Precision))
print('Recall:'+ str(Recall))
print('F-score:'+ str(Fscore))

####################Compute and display confusion matrix#####################
confusion_mat = confusion_matrix(test_y, pred_y)
plot_sonfusion_matrix(confusion_mat, classes=range(2))
####################ROC AUC Score############################################
print("AUC_ROC_Score:", roc_auc_score(test_y,pred_y))
