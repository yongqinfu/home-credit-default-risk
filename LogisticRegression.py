import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix,precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from imblearn.over_sampling import SMOTE
import statsmodels.api as sm
import itertools
from matplotlib import pyplot as plt

def plot_sonfusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment='center', color='white' if cm[i, j] > thresh else 'black')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predict label')

test_X = pd.read_csv('test_X.csv')
train_X = pd.read_csv('train_X.csv', header=0)
train_y = pd.read_csv('train_y.csv')
test_y = pd.read_csv('test_y.csv')
print(test_X.shape)
print(list(test_X.columns))
print(test_X.head())
print(train_X.head())
print(test_y.head())
print(train_y.head())

smo = SMOTE(random_state=42, n_jobs=-1)
x_sampling, y_sampling = smo.fit_sample(train_X, train_y)
print(x_sampling.shape)

logreg = LogisticRegression()
logreg.fit(train_X,train_y)

pred_y = logreg.predict(test_X)
print('Accuracy of logistic regression classifier on test set:{:.2f}'.format((logreg.score(test_X,test_y))))

print('Accuracy:'+ str(accuracy_score(test_y, pred_y)))
Precision,Recall,Fscore,none = precision_recall_fscore_support(test_y, pred_y, pos_label=1, average='binary')
print('Precision:'+ str(Precision))
print('Recall:'+ str(Recall))
print('F-score:'+ str(Fscore))

####################Compute and display confusion matrix#####################
confusion_mat = confusion_matrix(test_y, pred_y)
plot_sonfusion_matrix(confusion_mat, classes=range(2))
####################ROC AUC Score############################################
print("AUC_ROC_Score:", roc_auc_score(test_y,pred_y))

logit_roc_auc = roc_auc_score(test_y, logreg.predict(test_X))
fpr, tpr, thresholds = roc_curve(test_y, logreg.predict_proba(test_X)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('Log_ROC')
plt.show()
