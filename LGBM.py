import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def normalizingColumn(train, test, cName):
    scaler = StandardScaler().fit(train[[cName]])
    train[[cName]] = scaler.transform(train[[cName]])
    test[[cName]] = scaler.transform(test[[cName]])
    return train, test

data = pd.read_csv('F:\home-credit-default-risk/application_train.csv')

data = data.drop(columns=['SK_ID_CURR'])
data = pd.get_dummies(data, columns=['NAME_CONTRACT_TYPE'])
data = data.drop(data[data.CODE_GENDER=='XNA'].index)
data = pd.get_dummies(data, columns=['CODE_GENDER'])
data = pd.get_dummies(data, columns=['FLAG_OWN_CAR'])
data = pd.get_dummies(data, columns=['FLAG_OWN_REALTY'])
data = data.drop(columns=['NAME_TYPE_SUITE','NAME_INCOME_TYPE'])
data = pd.get_dummies(data, columns=['NAME_EDUCATION_TYPE'])
data = data.drop(data[data.NAME_FAMILY_STATUS=='Unknown'].index)
data = pd.get_dummies(data, columns=['NAME_FAMILY_STATUS'])
data = data.drop(columns=['NAME_HOUSING_TYPE','REGION_POPULATION_RELATIVE'])
data.DAYS_BIRTH = data.DAYS_BIRTH * (-1)
data.DAYS_EMPLOYED = data.DAYS_EMPLOYED * (-1)
data.DAYS_REGISTRATION = data.DAYS_REGISTRATION * (-1)
data.DAYS_ID_PUBLISH = data.DAYS_ID_PUBLISH * (-1)
data = data.drop(columns=['OWN_CAR_AGE'])
data.FLAG_PHONE = data.FLAG_MOBIL | data.FLAG_EMP_PHONE | data.FLAG_WORK_PHONE | data.FLAG_CONT_MOBILE | data.FLAG_PHONE
data = data.drop(columns=['FLAG_MOBIL','FLAG_EMP_PHONE','FLAG_WORK_PHONE','FLAG_CONT_MOBILE'])
data = data.drop(columns = ['OCCUPATION_TYPE'])
data = pd.get_dummies(data, columns=['REGION_RATING_CLIENT_W_CITY'])
data = pd.get_dummies(data,columns=['REGION_RATING_CLIENT'])
data = data.drop(columns=['WEEKDAY_APPR_PROCESS_START','HOUR_APPR_PROCESS_START'])
flag_address_not_match = data.REG_REGION_NOT_LIVE_REGION & data.REG_REGION_NOT_WORK_REGION & data.LIVE_REGION_NOT_WORK_REGION & data.REG_CITY_NOT_LIVE_CITY & data.REG_CITY_NOT_WORK_CITY & data.LIVE_CITY_NOT_WORK_CITY
data = data.drop(columns=['REG_REGION_NOT_LIVE_REGION','REG_REGION_NOT_WORK_REGION','LIVE_REGION_NOT_WORK_REGION','REG_CITY_NOT_LIVE_CITY','REG_CITY_NOT_WORK_CITY','LIVE_CITY_NOT_WORK_CITY'])
data['FLAG_ADDRESS_NOT_MATCH'] = flag_address_not_match
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Advertising','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Business Entity Type 1','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Business Entity Type 2','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Business Entity Type 3','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 1','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 2','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 3','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 4','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 5','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 6','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Trade: type 7','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Cleaning','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Construction','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Culture','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Hotel','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Housing','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Mobile','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Realtor','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Restaurant','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Telecom','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Transport: type 1','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Transport: type 2','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Transport: type 3','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Transport: type 4','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Bank','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Insurance','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Security','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Services','Business')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 1','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 2','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 3','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 4','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 5','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 6','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 7','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 8','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 9','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 10','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 11','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 12','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Industry: type 13','Industry')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Electricity','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Emergency','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Government','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Kindergarten','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Legal Services','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Medicine','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Military','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Police','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Postal','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Religion','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('School','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Security Ministries','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('University','Public_Services')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('XNA','Other')
data.ORGANIZATION_TYPE = data.ORGANIZATION_TYPE.replace('Self-employed','Other')
data = pd.get_dummies(data,columns=['ORGANIZATION_TYPE'])
data = data.drop(columns=['EXT_SOURCE_1'])


data = data.drop(columns=['APARTMENTS_AVG','BASEMENTAREA_AVG','YEARS_BEGINEXPLUATATION_AVG','YEARS_BUILD_AVG','COMMONAREA_AVG','ELEVATORS_AVG','ENTRANCES_AVG',
'FLOORSMAX_AVG','FLOORSMIN_AVG','LANDAREA_AVG','LIVINGAPARTMENTS_AVG','LIVINGAREA_AVG','NONLIVINGAPARTMENTS_AVG','NONLIVINGAREA_AVG','APARTMENTS_MODE',
'BASEMENTAREA_MODE','YEARS_BEGINEXPLUATATION_MODE','YEARS_BUILD_MODE','COMMONAREA_MODE','ELEVATORS_MODE','ENTRANCES_MODE','FLOORSMAX_MODE','FLOORSMIN_MODE',
'LANDAREA_MODE','LIVINGAPARTMENTS_MODE','LIVINGAREA_MODE','NONLIVINGAPARTMENTS_MODE','NONLIVINGAREA_MODE','APARTMENTS_MEDI','BASEMENTAREA_MEDI','YEARS_BEGINEXPLUATATION_MEDI',
'YEARS_BUILD_MEDI','COMMONAREA_MEDI','ELEVATORS_MEDI','ENTRANCES_MEDI','FLOORSMAX_MEDI','FLOORSMIN_MEDI','LANDAREA_MEDI','LIVINGAPARTMENTS_MEDI','LIVINGAREA_MEDI',
'NONLIVINGAPARTMENTS_MEDI','NONLIVINGAREA_MEDI','FONDKAPREMONT_MODE','HOUSETYPE_MODE','TOTALAREA_MODE','WALLSMATERIAL_MODE','EMERGENCYSTATE_MODE'])


data = data.drop(columns=['DEF_30_CNT_SOCIAL_CIRCLE','DEF_60_CNT_SOCIAL_CIRCLE'])
data = data.drop(columns=['DAYS_LAST_PHONE_CHANGE'])
data = data.drop(columns=['AMT_REQ_CREDIT_BUREAU_HOUR','AMT_REQ_CREDIT_BUREAU_DAY','AMT_REQ_CREDIT_BUREAU_WEEK','AMT_REQ_CREDIT_BUREAU_MON',
'AMT_REQ_CREDIT_BUREAU_QRT','AMT_REQ_CREDIT_BUREAU_YEAR'])

data = data.fillna(data.mean())
train_set, test_set = train_test_split(data, test_size = 0.2, random_state = 0)

train_X, test_X = train_set.drop(columns = ['TARGET']), test_set.drop(columns = ['TARGET'])
train_y, test_y = train_set['TARGET'], test_set['TARGET']

train_X, test_X = normalizingColumn(train_X, test_X, 'CNT_CHILDREN')
train_X, test_X = normalizingColumn(train_X, test_X, 'AMT_INCOME_TOTAL')
train_X, test_X = normalizingColumn(train_X, test_X, 'AMT_CREDIT')
train_X, test_X = normalizingColumn(train_X, test_X, 'AMT_ANNUITY')
train_X, test_X = normalizingColumn(train_X, test_X, 'AMT_GOODS_PRICE')
train_X, test_X = normalizingColumn(train_X, test_X, 'DAYS_BIRTH')
train_X, test_X = normalizingColumn(train_X, test_X, 'DAYS_EMPLOYED')
train_X, test_X = normalizingColumn(train_X, test_X, 'DAYS_REGISTRATION')
train_X, test_X = normalizingColumn(train_X, test_X, 'DAYS_ID_PUBLISH')
train_X, test_X = normalizingColumn(train_X, test_X, 'CNT_FAM_MEMBERS')
train_X, test_X = normalizingColumn(train_X, test_X, 'EXT_SOURCE_2')
train_X, test_X = normalizingColumn(train_X, test_X, 'EXT_SOURCE_3')
train_X, test_X = normalizingColumn(train_X, test_X, 'OBS_30_CNT_SOCIAL_CIRCLE')
train_X, test_X = normalizingColumn(train_X, test_X, 'OBS_60_CNT_SOCIAL_CIRCLE')

#np.savetxt('F:\home-credit-default-risk/train_X.csv', train_X, delimiter =',')
#np.savetxt('F:\home-credit-default-risk/test_X.csv', test_X, delimiter =',')
#np.savetxt('F:\home-credit-default-risk/train_y.csv', train_y, delimiter =',')
#np.savetxt('F:\home-credit-default-risk/test_y.csv', test_y, delimiter =',')

################################################################################################
#LGB
import lightgbm as lgb
train_data=lgb.Dataset(train_X,label=train_y)
test_data=lgb.Dataset(test_X,label=test_y)

from imblearn.over_sampling import SMOTE
smo = SMOTE(random_state=42, n_jobs=-1)
x_sampling, y_sampling = smo.fit_sample(train_X, train_y)
print(x_sampling.shape)
train_data2=lgb.Dataset(x_sampling,label=y_sampling)

params = {'boosting_type': 'gbdt',
          'max_depth' : 7,
          'num_leaves': 64,
          'objective': 'binary',
          'nthread': 5,
          'learning_rate': 0.05,
          'max_bin': 512,
          'subsample_for_bin': 200,
          'subsample': 1,
          'subsample_freq': 1,
          'colsample_bytree': 0.8,
          'reg_alpha': 5,
          'reg_lambda': 10,
          'min_split_gain': 0.5,
          'min_child_weight': 1,
          'min_child_samples': 5,
          'scale_pos_weight': 1,
          'num_class' : 1,
          'metric' : 'auc'
          }
model = lgb.train(params,
                 train_data2,
                 2500,
                 valid_sets=test_data,
                 early_stopping_rounds= 40,
                 verbose_eval= 10
                 )
pred_y = model.predict(test_X)
pred_y[pred_y>0.6]=1
pred_y[pred_y<=0.6]=0


##############################################################################################
#Evaluation
from sklearn.metrics import confusion_matrix,precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
import itertools


#Function plot_sonfusion_matrix from https://blog.csdn.net/mago2015/article/details/86426942
def plot_sonfusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment='center', color='white' if cm[i, j] > thresh else 'black')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predict label')
    

####################Compute Accuracy, Precision, Recall, F-score########################
print('Accuracy:'+ str(accuracy_score(test_y, pred_y)))
Precision,Recall,Fscore,none = precision_recall_fscore_support(test_y, pred_y, pos_label=1, average='binary') 
print('Precision:'+ str(Precision))
print('Recall:'+ str(Recall))
print('F-score:'+ str(Fscore))

####################Compute and display confusion matrix#####################
confusion_mat = confusion_matrix(test_y, pred_y)
plot_sonfusion_matrix(confusion_mat, classes=range(2))
####################ROC AUC Score############################################
print("AUC_ROC_Score:", roc_auc_score(test_y,pred_y))
