import pandas as pd
import numpy as np
import keras
import keras.backend as K
from sklearn.metrics import confusion_matrix,precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.layers import Activation
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras.callbacks import ReduceLROnPlateau
import itertools
from matplotlib import pyplot as plt
import os

#Function plot_sonfusion_matrix from https://blog.csdn.net/mago2015/article/details/86426942
def plot_sonfusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment='center', color='white' if cm[i, j] > thresh else 'black')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predict label')
    
####################Load Data#####################
train_X = pd.read_csv('/content/train_X.csv')
test_X = pd.read_csv('/content/test_X.csv')
train_y = pd.read_csv('/content/train_y.csv')
test_y = pd.read_csv('/content/test_y.csv')
train_y = keras.utils.to_categorical(train_y, 2)
test_y = keras.utils.to_categorical(test_y, 2)
####################Classification Model#####################
def learningRate_schedule(epoch):
    learningRate = 1e-3
    if epoch > 180:
        learningRate *= 0.5e-3
    elif epoch > 160:
        learningRate *= 1e-3
    elif epoch > 120:
        learningRate *= 1e-2
    elif epoch > 80:
        learningRate *= 1e-1
    print('The learning rate is: ', learningRate)
    return learningRate

save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'NN_%s_model.{epoch:03d}.h5'
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
filepath = os.path.join(save_dir, model_name)

checkpoint = ModelCheckpoint(filepath=filepath,
                             monitor='val_loss',
                             verbose=1,
                             save_best_only=True)

lr_scheduler = LearningRateScheduler(learningRate_schedule)

lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1),
                               cooldown=0,
                               patience=5,
                               min_lr=0.5e-6)

callbacks = [checkpoint, lr_reducer, lr_scheduler]

def getPrecision(y_true, y_pred):
    TP = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))#TP
    N = (-1)*K.sum(K.round(K.clip(y_true-K.ones_like(y_true), -1, 0)))#N
    TN=K.sum(K.round(K.clip((y_true-K.ones_like(y_true))*(y_pred-K.ones_like(y_pred)), 0, 1)))#TN
    FP=N-TN
    precision = TP / (TP + FP + K.epsilon())#TT/P
    return precision

def getRecall(y_true, y_pred):
    TP = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))#TP
    P=K.sum(K.round(K.clip(y_true, 0, 1)))
    FN = P-TP #FN=P-TP
    recall = TP / (TP + FN + K.epsilon())#TP/(TP+FN)
    return recall

def build_model():
    model = models.Sequential()
    model.add(layers.Dense(128,activation = 'relu', input_shape =
    (train_X.shape[1],)))
    model.add(layers.Dense(128, activation = 'relu'))
    model.add(layers.Dense(256, activation = 'relu'))
    model.add(layers.Dense(256, activation = 'relu'))
    model.add(layers.Dense(512, activation = 'relu'))
    model.add(layers.Dense(512, activation = 'relu'))
    model.add(layers.Dense(2, activation = 'softmax'))
    model.compile(optimizer=Adam(lr=learningRate_schedule(0)) ,loss='categorical_crossentropy', metrics = ['acc',getRecall,getPrecision])
    return model

model = build_model()
model.fit(train_X, train_y,
          batch_size=512,
          epochs=200,
          validation_data=(test_X, test_y),
          shuffle=True,
          callbacks=callbacks,
          class_weight=class_weight)

####################Compute Prediction#######################
pred_y = model.predict_classes(test_X)

np.savetxt('/content/pred_y.csv', pred_y, delimiter = ',')
