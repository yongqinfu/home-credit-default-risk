import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix,precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import statsmodels.api as sm
import itertools
from matplotlib import pyplot as plt

def plot_sonfusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment='center', color='white' if cm[i, j] > thresh else 'black')
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predict label')

test_X = pd.read_csv('test_X.csv')
train_X = pd.read_csv('train_X.csv')
train_y = pd.read_csv('train_y.csv')
test_y = pd.read_csv('test_y.csv')

knn=KNeighborsClassifier(n_neighbors=5)

knn.fit(train_X,train_y)

pred_y = knn.predict(test_X)

print("Accuracy:", accuracy_score(test_y,pred_y))

print('Accuracy:'+ str(accuracy_score(test_y, pred_y)))
Precision,Recall,Fscore,none = precision_recall_fscore_support(test_y, pred_y, pos_label=1, average='binary')
print('Precision:'+ str(Precision))
print('Recall:'+ str(Recall))
print('F-score:'+ str(Fscore))

####################Compute and display confusion matrix#####################
confusion_mat = confusion_matrix(test_y, pred_y)
plot_sonfusion_matrix(confusion_mat, classes=range(2))
####################ROC AUC Score############################################
print("AUC_ROC_Score:", roc_auc_score(test_y,pred_y))

knn_roc_auc = roc_auc_score(test_y, pred_y)
fpr, tpr, thresholds = roc_curve(test_y, knn.predict_proba(test_X)[:,1])
plt.figure()
plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % knn_roc_auc)
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('knn_ROC')
plt.show()
